## **Description**

---

This apllication developed to show basic `CRUD` operations worked with postgres db. The FrontEnd part of the app developed on the basis of the React. Also used `RTK` for working with state, `Next.ui` for the simple design. BackEnd part based on `Nest.js + Postgres` and `Typeorm`. `rss-parser` package used for parsing RSS files and work by `CRON` tasks. Endpoints secured by `Passport JWT` strategy. Also used `Swagger` for api documentation.

## **Prerequisites**

---

- node >= 14.17
- docker
- npm
- win-node-env (If you use OS Windows)

## **Run application**

---

1. If you use OS Windows you need to install `win-node-env` package. You can do it by the following command (it`s for developing process):

   ```properties
   npm install -g win-node-env
   ```

2. Change your credential in the `.env` file. Also you can use default but it`s not secure.

3. Setup your `docker` image by `docker-compose` file, running next command:

   ```properties
   npm run services:start
   ```

4. Install your packages for BE part and for FE part open the `src/client` folder:

   ```properties
   npm i
   ```

5. Next step, you need to run migration for database:

   ```properties
   npm run migration:latest
   ```

6. After instalation in the `src/client` run next command:

   ```properties
   npm run build
   ```

7. In the root folder run next command for starting server:

   ```properties
   npm run start
   ```

8. Now you can open `http://localhost:4000` to see the result.
