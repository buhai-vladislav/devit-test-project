import { createBrowserRouter } from 'react-router-dom';
import { Login } from '../components/Login';
import { RootLayout } from '../layouts';
import { SignUp } from '../components/SignUp/SignUp';
import { ProtectedRoute } from '../shared/ProtectedRoute/ProtectedRoute';
import { Posts } from '../components/Posts';
import { PostForm } from '../components/PostForm';

const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    children: [
      {
        index: true,
        element: <h2>Welcome to CrudRss!</h2>,
      },
      {
        path: 'login',
        element: <Login />,
      },
      {
        path: 'signup',
        element: <SignUp />,
      },
      {
        path: 'posts',
        element: (
          <ProtectedRoute>
            <Posts />
          </ProtectedRoute>
        ),
      },
      {
        element: <PostForm />,
        path: '/posts/create',
      },
      {
        element: <PostForm />,
        path: '/posts/:postId',
      },
    ],
  },
]);

export { router };
