export const DEFAULT_ERROR_MESSAGE = 'Something went wrong, try again!';
export const USER_NOT_FOUND_MESSAGE = 'Invalid login or password.';

export const AUTH_INPUT_WIDTH = '250px';

export const INITIAL_PAGE = 1;
export const LIMIT_PER_PAGE = 6;

export const DEFAULT_POST_IMAGE_URL =
  'https://play-lh.googleusercontent.com/Ew7HkAyuZeKrb93Cjhay-oUm5iJFA808RcRu_9ys2zqbZHPq3yceN_kL6Wo5Yb1DcCEC';
