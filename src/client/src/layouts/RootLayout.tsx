import { Outlet, useNavigate } from 'react-router-dom';
import { Box } from '../shared';
import { Header } from '../components/Header';
import { useEffect } from 'react';
import { useGetSelfQuery } from '../store/api/main.api';
import { useAppDispatch } from '../store/hooks/hooks';
import { setUser, resetUser } from '../store/reduser/user';

export default function RootLayout() {
  const dispatch = useAppDispatch();
  const { data, isError } = useGetSelfQuery({});
  const navigate = useNavigate();

  useEffect(() => {
    if (data && data.data) {
      dispatch(setUser(data.data));
      navigate('/posts');
    }
    if (isError) {
      dispatch(resetUser());
      navigate('/login');
    }
  }, [data, isError]);
  return (
    <>
      <Header />
      <Box
        css={{
          height: '100%',
          dflex: 'center',
        }}
      >
        <main>
          <Outlet />
        </main>
      </Box>
    </>
  );
}
