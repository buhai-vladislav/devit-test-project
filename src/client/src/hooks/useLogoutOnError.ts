import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAppDispatch } from '../store/hooks/hooks';
import { resetUser } from '../store/reduser/user';

export function useLogoutOnError(isError: boolean) {
  const dispathc = useAppDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    if (isError) {
      dispathc(resetUser());
      localStorage.removeItem('token');
      navigate('/login');
    }
  }, [isError]);
}
