import { FC, useEffect } from 'react';
import { Navigate, Outlet } from 'react-router-dom';
import { toast } from 'react-toastify';
import { useGetSelfQuery } from '../../store/api/main.api';
import { useAppDispatch, useAppSelector } from '../../store/hooks/hooks';
import { resetUser, setUser } from '../../store/reduser/user';
import { IPrivateRouteProps } from './ProtectedRoute.props';

export const ProtectedRoute: FC<IPrivateRouteProps> = ({
  children,
  redirectPath = '/',
}) => {
  const { user } = useAppSelector((state) => state.user);

  if (!user) {
    return <Navigate to={redirectPath} replace />;
  }

  return children ?? <Outlet />;
};
