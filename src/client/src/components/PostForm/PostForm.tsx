import {
  Badge,
  Button,
  FormElement,
  Input,
  Loading,
  Modal,
  Text,
  Textarea,
} from '@nextui-org/react';
import { nanoid } from '@reduxjs/toolkit';
import { useFormik } from 'formik';
import { KeyboardEvent, useEffect, useState } from 'react';
import ScrollContainer from 'react-indiana-drag-scroll';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { toast, ToastOptions } from 'react-toastify';
import { object, string } from 'yup';
import { IErrorItem, useErrorToast } from '../../hooks/useErrorToast';
import {
  useCreatePostMutation,
  useDeletePostMutation,
  useLazyGetPostByIdQuery,
  useUpdatePostMutation,
} from '../../store/api/main.api';
import { useAppDispatch, useAppSelector } from '../../store/hooks/hooks';
import { resetUser } from '../../store/reduser/user';
import { HttpStatus } from '../../types/HttpStatus';
import { DEFAULT_ERROR_MESSAGE } from '../../utils/constants';
import { CrossIcon } from './styles/CrossIcon';
import { PostFormWrapper } from './styles/PostFormWrapper';

import type { IResponse } from '../../types/Response';
import type { IMutation } from '../../types/Mutation';
import type { ICategoryItem, ICreateFormValues } from './PostForm.props';
import type { IPostBody, IPost, IAffectedResponse } from '../../types/Posts';

const ValidationSchema = object({
  title: string().required('Title is required!'),
  description: string().required('Description is required!'),
  thumbnail: string()
    .url('Is not a url format!')
    .required('Thumbnail is required!'),
});

export const PostForm = () => {
  const { user } = useAppSelector((state) => state.user);
  const [categories, setCategories] = useState<ICategoryItem[]>([]);
  const [isOpened, setIsOpened] = useState(false);
  const [createPost, { error, isLoading }] = useCreatePostMutation();
  const [updatePost, { error: updateError, isLoading: isPostUpdating }] =
    useUpdatePostMutation();
  const [deletePost, { isLoading: isPostDeleting, error: deleteError }] =
    useDeletePostMutation();
  const [getPostById, { isLoading: isPostLoading, data: post }] =
    useLazyGetPostByIdQuery();
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const { postId } = useParams();

  const toastOptions: ToastOptions = {
    position: 'bottom-center',
    type: 'error',
  };

  const onSubmit = async ({
    description,
    thumbnail,
    title,
  }: ICreateFormValues) => {
    const mappedCat = categories.map(({ title }) => title);
    const link = `https://lifehacker.com/${title
      .toLowerCase()
      .replaceAll(' ', '-')}`;

    const body: IPostBody = {
      categories: mappedCat,
      description,
      thumbnail,
      title,
      link,
    };

    if (postId) {
      const response: IMutation<IResponse<IAffectedResponse>> =
        await updatePost({ id: postId, ...body });

      if (response.data?.data) {
        toast('Post successfully updated.', {
          ...toastOptions,
          type: 'success',
        });
      }
    } else {
      const response: IMutation<IResponse<IPost>> = await createPost({
        id: nanoid(),
        ...body,
      });

      if (response.data?.statusCode === HttpStatus.CREATED) {
        toast('New post successfully creted!', {
          ...toastOptions,
          type: 'success',
        });
        navigate('/posts');
      }
    }
  };

  const onEnterPressHandler = ({
    key,
    currentTarget,
  }: KeyboardEvent<FormElement>) => {
    if (key === 'Enter') {
      setCategories((prevState) => [
        ...prevState,
        { key: nanoid(), title: currentTarget.value },
      ]);
      currentTarget.value = '';
    }
  };

  const deletePostHandler = async () => {
    if (postId) {
      const response: IMutation<IResponse<IAffectedResponse>> =
        await deletePost(postId);

      if (response.data?.data) {
        toast('Post successfully deleted.', {
          ...toastOptions,
          type: 'success',
        });
        navigate('/posts');
      }
    }
    closeModalHandler();
  };

  const openModalHandler = () => setIsOpened(true);
  const closeModalHandler = () => setIsOpened(false);

  const removeCategory = (itemKey: string) => {
    const filteredCategories = categories.filter(({ key }) => key !== itemKey);
    setCategories(filteredCategories);
  };

  const formik = useFormik<ICreateFormValues>({
    initialValues: {
      description: '',
      thumbnail: '',
      title: '',
    },
    onSubmit,
    validateOnChange: false,
    enableReinitialize: true,
    validationSchema: ValidationSchema,
  });

  const unauthErrorClb = () => {
    dispatch(resetUser());
    navigate('/login');
  };

  const errorItems: IErrorItem[] = [
    {
      status: HttpStatus.BAD_REQUEST,
      errorMessage: DEFAULT_ERROR_MESSAGE,
    },
    {
      status: HttpStatus.UNAUTHORIZED,
      errorMessage: 'Your validation token is valid, please relogin.',
      callback: unauthErrorClb,
    },
  ];

  useErrorToast(error, errorItems, toastOptions);
  useErrorToast(updateError, errorItems, toastOptions);
  useErrorToast(deleteError, errorItems, toastOptions);

  useEffect(() => {
    if (user) {
      formik.setFieldValue('creatorName', user.fullname);
    }
  }, []);

  useEffect(() => {
    if (postId) {
      getPostById(postId);
    }
  }, [postId]);

  useEffect(() => {
    if (post && post.data) {
      const { categories, title, description, thumbnail } = post.data;
      formik.setValues({ description, title, thumbnail });

      const items: ICategoryItem[] = categories.map((item) => ({
        key: nanoid(),
        title: item,
      }));
      setCategories(items);
    }
  }, [post]);

  return isPostLoading ? (
    <Loading size="xl" />
  ) : (
    <PostFormWrapper onSubmit={formik.handleSubmit}>
      <Text h3>{postId ? 'Post' : 'Create post'}</Text>
      <div className="scroll-wrapper">
        <Input
          className="input"
          label="Title"
          placeholder="Enter post title"
          name="title"
          value={formik.values.title}
          onChange={formik.handleChange}
          status={!!formik.errors.title ? 'error' : 'default'}
          helperText={formik.errors.title}
        />
        <Textarea
          placeholder="Write a post description"
          minRows={3}
          maxRows={3}
          label="Description"
          name="description"
          className="input"
          value={formik.values.description}
          onChange={formik.handleChange}
          status={!!formik.errors.description ? 'error' : 'default'}
          helperText={formik.errors.description}
        />
        <div className="category-block">
          <Input
            className="input"
            label="Category"
            placeholder="Enter post category and press 'Enter' to add one"
            name="category"
            onKeyDown={onEnterPressHandler}
          />
          <ScrollContainer
            vertical
            hideScrollbars={false}
            className="categories"
          >
            {categories.map(({ key, title }) => (
              <Badge key={key} color="primary">
                {title}
                <button onClick={() => removeCategory(key)} type="button">
                  <CrossIcon />
                </button>
              </Badge>
            ))}
          </ScrollContainer>
        </div>
        <Input
          className="input"
          label="Thumbnail"
          placeholder="https://imageholder/image.jpg"
          name="thumbnail"
          type="url"
          value={formik.values.thumbnail}
          onChange={formik.handleChange}
          status={!!formik.errors.thumbnail ? 'error' : 'default'}
          helperText={formik.errors.thumbnail}
        />
      </div>
      <Button.Group>
        <Button
          className={postId ? 'submit-button save' : 'submit-button'}
          auto
          flat
          type="button"
          disabled={
            !formik.dirty || isLoading || isPostUpdating || isPostDeleting
          }
          onClick={() => formik.submitForm()}
        >
          {isLoading || isPostUpdating ? (
            <Loading type="points-opacity" color="currentColor" size="sm" />
          ) : (
            <span>{postId ? 'Save' : 'Create'}</span>
          )}
        </Button>
        {postId && (
          <Button className="delete" type="button" onPress={openModalHandler}>
            Delete
          </Button>
        )}
      </Button.Group>
      <Modal
        closeButton
        aria-labelledby="modal-title"
        open={isOpened}
        onClose={closeModalHandler}
        css={{ alignItems: 'center', justifyContent: 'center' }}
      >
        <Text h3 css={{ width: '250px' }}>
          Are you sure to delete this post?
        </Text>
        <Button.Group css={{ width: '260px' }}>
          <Button flat onPress={closeModalHandler} css={{ width: '130px' }}>
            Cancel
          </Button>
          <Button
            disabled={isPostDeleting || isPostUpdating}
            onPress={deletePostHandler}
            css={{ width: '130px' }}
          >
            Aplly
          </Button>
        </Button.Group>
      </Modal>
    </PostFormWrapper>
  );
};
