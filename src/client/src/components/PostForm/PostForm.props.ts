interface ICreateFormValues {
  title: string;
  description: string;
  thumbnail: string;
}

interface ICategoryItem {
  key: string;
  title: string;
}

export type { ICreateFormValues, ICategoryItem };
