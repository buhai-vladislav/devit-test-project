import { styled } from '@nextui-org/react';

export const PostFormWrapper = styled('form', {
  width: '500px',
  borderRadius: '25px',
  boxShadow: '$lg',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  h3: {
    marginBottom: '0',
    marginTop: '10px',
  },
  '.modal': {
    alignItems: 'center',
    justifyContent: 'center',
    h3: {
      width: '250px',
    },
  },
  '.submit-button': {
    width: '400px',
    marginBottom: '15px',
  },
  '.save': {
    width: '200px !important',
  },
  '.delete': {
    backgroundColor: '$error',
    width: '200px',
  },
  '.scroll-wrapper': {
    minHeight: '450px',
    dflex: 'center',
    flexDirection: 'column',
    gap: '15px',
    '.input': {
      width: '400px',
    },
    /* width */
    '&::-webkit-scrollbar': {
      width: '10px',
    },

    /* Track */
    '&::-webkit-scrollbar-track': {
      background: '#f1f1f1',
      borderRadius: '10px',
    },

    /* Handle */
    '&::-webkit-scrollbar-thumb': {
      background: '#888',
      borderRadius: '10px',
    },

    /* Handle on hover */
    '&::-webkit-scrollbar-thumb:hover': {
      background: '#555',
    },
    '.categories': {
      display: 'flex',
      flexWrap: 'wrap',
      width: '400px',
      maxHeight: '60px',
      marginTop: '10px',
      /* width */
      '&::-webkit-scrollbar': {
        width: '10px',
      },

      /* Track */
      '&::-webkit-scrollbar-track': {
        background: '#f1f1f1',
        borderRadius: '10px',
      },

      /* Handle */
      '&::-webkit-scrollbar-thumb': {
        background: '#888',
        borderRadius: '10px',
      },

      /* Handle on hover */
      '&::-webkit-scrollbar-thumb:hover': {
        background: '#555',
      },
      button: {
        background: 'none',
        border: 'none',
        height: '12px',
      },
      svg: {
        height: '12px',
        width: '12px',
        fill: 'white',
        cursor: 'pointer',
      },
    },
  },
});
