import { Card, Avatar, Badge, Text } from '@nextui-org/react';
import { FC } from 'react';
import { useNavigate } from 'react-router-dom';
import { IPost } from '../../../../types/Posts';
import { DEFAULT_POST_IMAGE_URL } from '../../../../utils/constants';
import { IPostCardsProps } from './PostCards.props';

export const PostCards: FC<IPostCardsProps> = ({ cards }) => {
  const navigate = useNavigate();
  return (
    <>
      {cards.length > 0 ? (
        cards.map((card: IPost) => (
          <Card
            variant="flat"
            className="card"
            isHoverable
            isPressable
            onPress={() =>
              navigate(`/posts/${card.id}`, {
                state: card,
              })
            }
          >
            <Card.Body>
              <div className="main-wrapper">
                <Avatar
                  src={
                    card.thumbnail === ''
                      ? DEFAULT_POST_IMAGE_URL
                      : card.thumbnail
                  }
                  size="xl"
                  squared
                />
                <div className="body-wrapper">
                  <Text h5>
                    <span className="title-start">Post: </span>
                    {card.title}
                  </Text>
                  <Text h6>
                    <span className="author">Authored by: </span>
                    {card.creatorName}
                  </Text>
                  <div className="categories">
                    {card.categories.slice(0, 5).map((category) => (
                      <Badge enableShadow disableOutline color="primary">
                        {category.slice(0, 11)}
                      </Badge>
                    ))}
                  </div>
                </div>
              </div>
            </Card.Body>
          </Card>
        ))
      ) : (
        <Text h3>No posts to show, try again.</Text>
      )}
    </>
  );
};
