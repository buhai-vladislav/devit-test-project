import { IPost } from '../../../../types/Posts';

interface IPostCardsProps {
  cards: IPost[];
}

export type { IPostCardsProps };
