import { FormElement, SelectionMode } from '@nextui-org/react';
import {
  ChangeEvent,
  Dispatch,
  Key,
  ReactElement,
  SetStateAction,
} from 'react';

type DropdownValue = 'all' | Set<Key>;

interface IDropdownItem {
  key: string;
  title: string;
}

interface IDropdownMenu {
  title: string;
  disallowEmptySelection?: boolean;
  value: DropdownValue;
  setValue: Dispatch<SetStateAction<DropdownValue>>;
  items: IDropdownItem[];
  selectionMode?: SelectionMode;
}

interface IPostsHeaderProps {
  dropdowns: IDropdownMenu[];
  onInputChange: (event: ChangeEvent<FormElement>) => void;
  className?: string;
  inputPlaceholder?: string;
  buttons?: ReactElement[];
}

export type { IPostsHeaderProps, DropdownValue, IDropdownItem, IDropdownMenu };
