import { PostsHeader } from './PostsHeader';
import type {
  DropdownValue,
  IDropdownItem,
  IDropdownMenu,
  IPostsHeaderProps,
} from './PostsHeader.props';

export { PostsHeader };
export type { DropdownValue, IDropdownItem, IDropdownMenu, IPostsHeaderProps };
