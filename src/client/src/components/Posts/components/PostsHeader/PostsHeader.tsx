import { Dropdown, Input } from '@nextui-org/react';
import { FC } from 'react';
import { IPostsHeaderProps } from './PostsHeader.props';

export const PostsHeader: FC<IPostsHeaderProps> = ({
  dropdowns,
  onInputChange,
  className = 'search-wrapper',
  inputPlaceholder = 'Search by title',
  buttons,
}) => {
  return (
    <div className={className}>
      <Input
        placeholder={inputPlaceholder}
        fullWidth
        onChange={onInputChange}
      />
      <>
        {dropdowns.map(
          ({
            title,
            value,
            setValue,
            items,
            disallowEmptySelection,
            selectionMode,
          }) => (
            <Dropdown key={title.replace(' ', '_').toLowerCase()}>
              <Dropdown.Button flat>{title}</Dropdown.Button>
              <Dropdown.Menu
                aria-label={title}
                selectionMode={selectionMode}
                disallowEmptySelection={disallowEmptySelection}
                selectedKeys={value}
                onSelectionChange={setValue}
              >
                {items.map(({ key, title }) => (
                  <Dropdown.Item key={key}>{title}</Dropdown.Item>
                ))}
              </Dropdown.Menu>
            </Dropdown>
          ),
        )}
        {buttons}
      </>
    </div>
  );
};
