import { styled } from '@nextui-org/react';

export const PostsWrapper = styled('section', {
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'space-between',
  '.posts-container': {
    gap: '15px',
    justifyContent: 'center',
  },
  '.scroll-container': {
    height: '500px',
    minWidth: '300px',
    display: 'flex',
    alignItems: 'center',
    maxWidth: '1300px',
  },
  '.main-wrapper': {
    display: 'flex',
    gap: '15px',
    '.nextui-image-container': {
      margin: 0,
    },
    '.title-start, .author': {
      fontWeight: '700',
    },
    '.categories': {
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      gap: '5px',
    },
    h5: {
      minHeight: '80px',
    },
  },
  '.card': {
    width: '350px',
  },
  '.search-wpapper': {
    display: 'flex',
    flexDirection: 'row',
    gap: '15px',
    width: '1000px',
  },
});
