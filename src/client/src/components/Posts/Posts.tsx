import {
  Button,
  FormElement,
  Grid,
  Loading,
  Pagination,
} from '@nextui-org/react';
import { ChangeEvent, Key, useEffect, useMemo, useState } from 'react';
import { useLazyGetPostsQuery } from '../../store/api/main.api';
import { IPaginationMeta } from '../../types/Posts';
import { PostsWrapper } from './styles/PostsWrapper';
import ScrollContainer from 'react-indiana-drag-scroll';
import { PostCards } from './components/PostCards';
import { useDebounce } from 'use-debounce';
import { INITIAL_PAGE } from '../../utils/constants';
import { useLogoutOnError } from '../../hooks/useLogoutOnError';
import { IPostsHeaderProps, PostsHeader } from './components/PostsHeader';
import { useNavigate } from 'react-router-dom';

export const Posts = () => {
  const [getPosts, { data: posts, isLoading, isError }] =
    useLazyGetPostsQuery();
  const [pagintionData, setPaginationData] = useState<
    IPaginationMeta | undefined
  >();
  const [currentPage, setCurrentPage] = useState<number>(INITIAL_PAGE);
  const [rawSearchValue, setSearchValue] = useState<string | undefined>('');
  const [filterBy, setFilterBy] = useState<'all' | Set<Key>>(new Set(['all']));
  const [sortBy, setSortBy] = useState<'all' | Set<Key>>(new Set(['ASC']));
  const navigate = useNavigate();

  const selectedFilterValue = useMemo(() => {
    setCurrentPage(INITIAL_PAGE);
    return Array.from(filterBy).join('');
  }, [filterBy]);
  const selectedSortByValue = useMemo(() => {
    setCurrentPage(INITIAL_PAGE);
    return Array.from(sortBy).join('');
  }, [sortBy]);

  const [searchValue] = useDebounce(rawSearchValue, 700);

  const onPageChange = (page: number) => setCurrentPage(page);
  const onSearchChange = ({ target }: ChangeEvent<FormElement>) =>
    setSearchValue(target.value);
  const handleCreate = () => navigate('create');

  useEffect(() => {
    getPosts({});
  }, []);

  useEffect(() => {
    if (posts?.data && posts.data.meta) {
      const { meta } = posts.data;
      setPaginationData(meta);
    }
    if (posts?.data && posts.data.items.length === 0) {
      setPaginationData(undefined);
      setCurrentPage(INITIAL_PAGE);
    }
  }, [posts]);

  useEffect(() => {
    getPosts({
      page: currentPage,
      search: searchValue,
      filter: selectedFilterValue,
      sortBy: selectedSortByValue,
    });
  }, [currentPage, searchValue, selectedFilterValue, selectedSortByValue]);

  useLogoutOnError(isError);

  const headerProps: IPostsHeaderProps = {
    onInputChange: onSearchChange,
    className: 'search-wpapper',
    inputPlaceholder: 'Search by title',
    dropdowns: [
      {
        title: 'Filter by',
        value: filterBy,
        setValue: setFilterBy,
        selectionMode: 'single',
        items: [
          {
            key: 'OWN_POSTS',
            title: 'Own posts',
          },
        ],
      },
      {
        title: 'Sort by',
        value: sortBy,
        setValue: setSortBy,
        selectionMode: 'single',
        disallowEmptySelection: true,
        items: [
          {
            key: 'ASC',
            title: 'Ascending',
          },
          {
            key: 'DESC',
            title: 'Descending',
          },
        ],
      },
    ],
    buttons: [
      <Button auto onPress={handleCreate}>
        Create post
      </Button>,
    ],
  };

  return (
    <PostsWrapper>
      <PostsHeader {...headerProps} />
      <ScrollContainer
        vertical
        horizontal={false}
        className="scroll-container"
        hideScrollbars={false}
      >
        <Grid.Container className="posts-container">
          {isLoading ? (
            <Loading size="xl" className="spiner" />
          ) : (
            <PostCards cards={posts?.data?.items ?? []} />
          )}
        </Grid.Container>
      </ScrollContainer>
      {pagintionData && (
        <Pagination
          total={pagintionData.totalPages}
          initialPage={1}
          onChange={onPageChange}
          page={currentPage}
        />
      )}
    </PostsWrapper>
  );
};
