import {
  Avatar,
  Button,
  Modal,
  Navbar,
  Popover,
  Text,
} from '@nextui-org/react';
import { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useAppDispatch, useAppSelector } from '../../store/hooks/hooks';
import { resetUser } from '../../store/reduser/user';
import { Logo } from './styles/Logo';

export const Header = () => {
  const { user } = useAppSelector((state) => state.user);
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const logoutHandler = () => {
    dispatch(resetUser());
    localStorage.removeItem('token');
    navigate('/login');
  };

  return (
    <Navbar isBordered variant="sticky">
      <Navbar.Brand>
        <Logo />
        <Text b color="inherit" hideIn="xs">
          <Link to="/">CrudRss</Link>
        </Text>
      </Navbar.Brand>
      <Navbar.Content>
        {!user ? (
          <>
            <Button auto light>
              <Link to="login">Login</Link>
            </Button>
            <Button auto flat>
              <Link to="signup">Sign Up</Link>
            </Button>
          </>
        ) : (
          <>
            <Link to="posts">
              <Text h5 css={{ marginBottom: '0' }}>
                Posts
              </Text>
            </Link>
            <Popover placement="left">
              <Popover.Trigger>
                <Avatar
                  text={user.fullname}
                  squared
                  bordered
                  css={{ cursor: 'pointer' }}
                />
              </Popover.Trigger>
              <Popover.Content>
                <Button size="sm" onPress={logoutHandler}>
                  Log out
                </Button>
              </Popover.Content>
            </Popover>
          </>
        )}
      </Navbar.Content>
    </Navbar>
  );
};
