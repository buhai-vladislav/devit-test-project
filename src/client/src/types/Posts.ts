import { IUser } from './Auth';

interface IVersionedEntity {
  createdAt: Date;
  updatedAt: Date;
  rowVersion: number;
}

interface IPost extends IVersionedEntity {
  id: string;
  title: string;
  link: string;
  categories: string[];
  description: string;
  creatorName: string;
  thumbnail: string;
  creator?: IUser;
}

interface IPaginationMeta {
  currentPage: number;
  itemCount: number;
  itemsPerPage: number;
  totalItems: number;
  totalPages: number;
}

interface IPostsData {
  items: IPost[];
  meta: IPaginationMeta;
}

enum Filter {
  OWN_POSTS = 'OWN_POSTS',
}

enum SortBy {
  DESC = 'DESC',
  ASC = 'ASC',
}

interface IGetPostProps {
  page?: number;
  limit?: number;
  search?: string;
  filter?: string;
  sortBy?: string;
}

interface IPostBody {
  id?: string;
  title: string;
  description: string;
  link: string;
  categories: string[];
  thumbnail: string;
}

interface IAffectedResponse {
  isAffected: boolean;
}

export type {
  IPaginationMeta,
  IPost,
  IPostsData,
  Filter,
  IGetPostProps,
  SortBy,
  IPostBody,
  IVersionedEntity,
  IAffectedResponse,
};
