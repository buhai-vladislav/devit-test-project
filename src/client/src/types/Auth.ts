import { IVersionedEntity } from './Posts';

interface IUser extends IVersionedEntity {
  id: string;
  fullname: string;
  email: string;
  role: string;
}

interface ISigninResponseData {
  user: IUser;
  token: string;
}

interface ILogin {
  email: string;
  password: string;
}

interface ISignUp extends ILogin {
  fullname: string;
}

export type { IUser, ISigninResponseData, ILogin, ISignUp };
