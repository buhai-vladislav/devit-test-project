import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';
import type {
  ILogin,
  ISigninResponseData,
  ISignUp,
  IUser,
} from '../../types/Auth';
import {
  IAffectedResponse,
  IPostBody,
  IGetPostProps,
  IPost,
  IPostsData,
} from '../../types/Posts';
import type { IResponse } from '../../types/Response';
import { INITIAL_PAGE, LIMIT_PER_PAGE } from '../../utils/constants';

const baseQuery = fetchBaseQuery({
  baseUrl: '/',
  prepareHeaders: (headers) => {
    const token = localStorage.getItem('token');

    if (token) {
      headers.set('authorization', `Bearer ${token}`);
    }

    return headers;
  },
});

export const mainApi = createApi({
  reducerPath: 'main',
  baseQuery,
  endpoints: (build) => ({
    signin: build.mutation<IResponse<ISigninResponseData>, ILogin>({
      query: ({ email, password }) => ({
        url: 'auth/signin',
        method: 'POST',
        body: {
          email,
          password,
        },
      }),
    }),
    signup: build.mutation<IResponse<IUser>, ISignUp>({
      query: ({ email, password, fullname }) => ({
        url: 'auth/signup',
        method: 'POST',
        body: {
          email,
          password,
          fullname,
        },
      }),
    }),
    getPosts: build.query<IResponse<IPostsData>, IGetPostProps>({
      query: ({
        page = INITIAL_PAGE,
        limit = LIMIT_PER_PAGE,
        filter,
        search,
        sortBy,
      }) => ({
        url: '/posts',
        method: 'GET',
        params: {
          page,
          limit,
          filter,
          search,
          sortBy,
        },
      }),
    }),
    getSelf: build.query<IResponse<IUser>, {}>({
      query: () => ({
        url: '/users/self',
        method: 'GET',
      }),
    }),
    createPost: build.mutation<IResponse<IPost>, IPostBody>({
      query: (body) => ({
        url: '/posts',
        method: 'POST',
        body,
      }),
    }),
    getPostById: build.query<IResponse<IPost>, string>({
      query: (id: string) => ({
        url: `/posts/${id}`,
        method: 'GET',
      }),
    }),
    updatePost: build.mutation<
      IResponse<IAffectedResponse>,
      Partial<IPostBody>
    >({
      query: ({ id, categories, description, thumbnail, title }) => ({
        url: `/posts/${id}`,
        method: 'PUT',
        body: {
          categories,
          title,
          description,
          thumbnail,
        },
      }),
    }),
    deletePost: build.mutation<IResponse<IAffectedResponse>, string>({
      query: (id) => ({
        url: `/posts/${id}`,
        method: 'DELETE',
      }),
    }),
  }),
});

export const {
  useSigninMutation,
  useSignupMutation,
  useLazyGetPostsQuery,
  useGetSelfQuery,
  useCreatePostMutation,
  useLazyGetPostByIdQuery,
  useUpdatePostMutation,
  useDeletePostMutation,
} = mainApi;
