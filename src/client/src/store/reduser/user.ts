import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ISigninResponseData, IUser } from '../../types/Auth';

const initialState: Partial<Omit<ISigninResponseData, 'token'>> = {
  user: undefined,
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUser: (state, action: PayloadAction<IUser>) => {
      state.user = action.payload;
    },
    resetUser: (state) => {
      state.user = undefined;
    },
  },
});

export const { setUser, resetUser } = userSlice.actions;
export default userSlice.reducer;
