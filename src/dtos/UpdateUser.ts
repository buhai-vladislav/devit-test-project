import { ApiProperty } from '@nestjs/swagger';
import {
  IsString,
  IsOptional,
  MaxLength,
  IsEmail,
  IsEnum,
} from 'class-validator';
import { UserRole } from 'src/types/UserRole';

export class UpdateUserDto {
  @ApiProperty({
    description: 'The fullname of the user to update.',
    maxLength: 50,
    required: false,
  })
  @IsString()
  @MaxLength(50)
  @IsOptional()
  fullname?: string;

  @ApiProperty({
    description: 'The email of the user to update.',
    required: false,
  })
  @IsEmail()
  @IsOptional()
  email?: string;

  @ApiProperty({
    description: 'The role of the user to update.',
    required: false,
  })
  @IsEnum(UserRole)
  @IsOptional()
  role?: UserRole;
}
