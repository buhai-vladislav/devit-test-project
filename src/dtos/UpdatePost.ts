import {
  IsString,
  IsArray,
  IsUrl,
  IsOptional,
  MaxLength,
} from 'class-validator';

export class UpdatePostDto {
  @IsString()
  @IsOptional()
  title?: string;

  @IsOptional()
  @IsUrl()
  link?: string;

  @IsArray()
  @IsOptional()
  categories?: string[];

  @IsOptional()
  @IsString()
  description?: string;

  @IsOptional()
  @IsString()
  @MaxLength(50)
  creatorName?: string;

  @IsOptional()
  @IsUrl()
  thumbnail?: string;
}
