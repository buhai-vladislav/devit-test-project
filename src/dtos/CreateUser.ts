import { ApiProperty } from '@nestjs/swagger';
import {
  IsString,
  IsNotEmpty,
  MaxLength,
  IsEmail,
  IsEnum,
} from 'class-validator';
import { UserRole } from 'src/types/UserRole';

export class CreateUserDto {
  @ApiProperty({
    description: 'The fullname of the new user.',
    maxLength: 50,
  })
  @IsString()
  @IsNotEmpty()
  @MaxLength(50)
  fullname: string;

  @ApiProperty({
    description: 'The email of the new user.',
  })
  @IsEmail()
  @IsNotEmpty()
  email: string;

  @ApiProperty({
    description: 'The raw password of the new user.',
  })
  @IsNotEmpty()
  @IsString()
  password: string;

  @ApiProperty({
    description: 'The role of the new user.',
    default: UserRole.ADMIN,
  })
  @IsEnum(UserRole)
  @IsNotEmpty()
  role: UserRole = UserRole.ADMIN;
}
