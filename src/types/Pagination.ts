import { IPaginationMetadata } from './PaginationMetadata';

export interface IPagination<Entity> {
  items: Entity[];
  meta: IPaginationMetadata;
}
