import { HttpStatus } from '@nestjs/common/enums';

export interface IResponseData<T> {
  statusCode: HttpStatus;
  message: string | object;
  data?: Partial<T> | T;
}
