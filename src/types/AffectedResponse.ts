export interface AffectedResponse {
  isAffected: boolean;
}
