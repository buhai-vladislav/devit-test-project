export interface IPaginationMetadata {
  itemsPerPage: number;
  totalItems: number;
  currentPage: number;
  totalPages: number;
  itemCount: number;
}
