export enum Filters {
  OWN_POSTS = 'own_posts',
}

export enum SortBy {
  DESC = 'DESC',
  ASC = 'ASC',
}

export interface IPostWhere {
  filter?: Filters;
  sortBy?: SortBy;
  searchString?: string;
  userId?: string;
}
