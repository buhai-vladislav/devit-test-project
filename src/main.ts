import { Logger, ValidationPipe } from '@nestjs/common';
import { NestFactory, Reflector } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { JwtAuthGuard } from './guards/JwtAuthGuard';
import { AppModule } from './modules/Application';
import { JWT_BEARER_SWAGGER_AUTH_NAME } from './utils/constants';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });

  const logger = new Logger('NestApplication');

  const reflector = app.get(Reflector);

  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalGuards(new JwtAuthGuard(reflector));

  const config = new DocumentBuilder()
    .setTitle('Test project API')
    .setDescription('The DevIt test project API endpoints documentation')
    .setVersion('1.0')
    .addBearerAuth(
      {
        type: 'http',
        scheme: 'bearer',
        bearerFormat: 'JWT',
        name: 'JWT',
        description: 'Enter JWT token',
        in: 'header',
      },
      JWT_BEARER_SWAGGER_AUTH_NAME, // This name here is important for matching up with @ApiBearerAuth() in the controller!
    )
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  logger.log(`Server started on the ${AppModule.port} port.`);
  await app.listen(AppModule.port || 3000);
}
bootstrap();
