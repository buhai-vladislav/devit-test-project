import {
  Body,
  Controller,
  Get,
  HttpStatus,
  Param,
  Put,
  Req,
  Res,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiOperation,
  ApiParam,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { Response } from 'express';
import { UpdateUserDto } from 'src/dtos/UpdateUser';
import { UserService } from 'src/services/User';
import { JWT_BEARER_SWAGGER_AUTH_NAME } from 'src/utils/constants';

@ApiTags('User')
@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @ApiOperation({
    description: 'Update the user by id',
    operationId: 'updateUser',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The user successfully updated',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: "The user wasn't updated or something went wrong",
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'The user login or password is not correct',
  })
  @ApiParam({
    name: 'userId',
    description: 'The id of the user to update.',
  })
  @ApiBody({ type: UpdateUserDto })
  @ApiBearerAuth(JWT_BEARER_SWAGGER_AUTH_NAME)
  @Put('/:userId')
  public async update(
    @Param('userId') userId: string,
    @Body() updateUserDto: UpdateUserDto,
    @Res() response: Response,
  ) {
    return this.userService.update(userId, updateUserDto, response);
  }

  @ApiOperation({
    description: 'Get user by token.',
    operationId: 'getYourSelf',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The user successfully returned',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: "The user wasn't returned or something went wrong",
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'The user unauthorized.',
  })
  @ApiBearerAuth(JWT_BEARER_SWAGGER_AUTH_NAME)
  @Get('/self')
  public async getYourSelf(@Req() request: any, @Res() response: Response) {
    const userId = request?.user?.userId;

    return this.userService.getOneByOptions({ id: userId }, response);
  }
}
