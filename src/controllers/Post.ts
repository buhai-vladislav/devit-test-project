import { DefaultValuePipe, HttpStatus, ParseIntPipe } from '@nestjs/common';
import {
  Body,
  Controller,
  Res,
  Post,
  Param,
  Get,
  Put,
  Delete,
  Req,
  Query,
} from '@nestjs/common/decorators';
import {
  ApiBearerAuth,
  ApiBody,
  ApiOperation,
  ApiParam,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { Response } from 'express';
import { CreatePostDto } from 'src/dtos/CreatePost';
import { UpdatePostDto } from 'src/dtos/UpdatePost';
import { PostService } from 'src/services/Post';
import { Filters, IPostWhere, SortBy } from 'src/types/PostsWhere';
import {
  DEFAULT_PAGINATION_PAGE,
  DEFAULT_PAGINATION_LIMIT,
  JWT_BEARER_SWAGGER_AUTH_NAME,
} from 'src/utils/constants';

@ApiTags('Posts')
@Controller('posts')
export class PostController {
  constructor(private readonly postService: PostService) {}

  @ApiOperation({
    description: 'Create a new post record.',
    operationId: 'createPost',
  })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'The post successfully created.',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: "The post wasn't created or something went wrong.",
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'The user login or password is not correct.',
  })
  @ApiBody({ type: CreatePostDto })
  @ApiBearerAuth(JWT_BEARER_SWAGGER_AUTH_NAME)
  @Post('/')
  public async create(
    @Body() createPostDto: CreatePostDto,
    @Res() response: Response,
    @Req() request: any,
  ) {
    const userId = request.user.userId;

    return this.postService.createPost(userId, createPostDto, response);
  }

  @ApiOperation({
    description: 'Get single post by id.',
    operationId: 'getSinglePost',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The post successfully returned.',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: "The post wasn't returned or something went wrong.",
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'The user login or password is not correct.',
  })
  @ApiParam({
    name: 'postId',
    description: 'The id of the post to get.',
  })
  @ApiBearerAuth(JWT_BEARER_SWAGGER_AUTH_NAME)
  @Get('/:postId')
  public async getById(
    @Param('postId') postId: string,
    @Res() response: Response,
  ) {
    return this.postService.getOneByOptions({ id: postId }, response);
  }

  @ApiOperation({
    description: 'Update the post record by id.',
    operationId: 'updatePost',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The post successfully updated.',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: "The post wasn't updated or something went wrong.",
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'The user login or password is not correct.',
  })
  @ApiParam({
    name: 'post',
    description: 'The id of the post to update.',
  })
  @ApiBody({ type: UpdatePostDto })
  @ApiBearerAuth(JWT_BEARER_SWAGGER_AUTH_NAME)
  @Put('/:postId')
  public async update(
    @Param('postId') postId: string,
    @Body() updatePostDto: UpdatePostDto,
    @Res() response: Response,
  ) {
    return this.postService.update(postId, updatePostDto, response);
  }

  @ApiOperation({
    description: 'Delete the post by id',
    operationId: 'deletePost',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The post successfully deleted.',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: "The post wasn't updated or something went wrong.",
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'The user login or password is not correct.',
  })
  @ApiParam({
    name: 'postId',
    description: 'The id of the post to delete.',
  })
  @ApiBearerAuth(JWT_BEARER_SWAGGER_AUTH_NAME)
  @Delete('/:postId')
  public async delete(
    @Param('postId') postId: string,
    @Res() response: Response,
  ) {
    return this.postService.delete({ id: postId }, response);
  }

  @ApiOperation({
    description: 'Get posts paged by params.',
    operationId: 'getByParams',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The posts successfully returned.',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: "Posts wasn't returned or something went wrong.",
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'The user login or password is not correct.',
  })
  @ApiQuery({
    description: 'The number of the current page. Default value is 1.',
    required: false,
    name: 'page',
  })
  @ApiQuery({
    description: 'The limit of the friends by page. Default value is 30.',
    required: false,
    name: 'limit',
  })
  @ApiQuery({
    description: 'The search text for searching by post title.',
    required: false,
    name: 'search',
  })
  @ApiQuery({
    description: 'The filter enum value.',
    required: false,
    name: 'filter',
    schema: {
      type: 'string',
      enum: ['OWN_POSTS'],
    },
  })
  @ApiQuery({
    description: 'The sort param for sorting by.',
    required: false,
    name: 'sortBy',
    schema: {
      type: 'string',
      enum: ['ASC', 'DESC'],
    },
  })
  @ApiBearerAuth(JWT_BEARER_SWAGGER_AUTH_NAME)
  @Get('/')
  public async getPaged(
    @Res() response: Response,
    @Req() request: any,
    @Query('page', new DefaultValuePipe(DEFAULT_PAGINATION_PAGE), ParseIntPipe)
    page: number = DEFAULT_PAGINATION_PAGE,
    @Query(
      'limit',
      new DefaultValuePipe(DEFAULT_PAGINATION_LIMIT),
      ParseIntPipe,
    )
    limit: number = DEFAULT_PAGINATION_LIMIT,
    @Query('search') searchString: string,
    @Query('filter') filter: string,
    @Query('sortBy') sortBy: string,
  ) {
    const where: IPostWhere = {
      sortBy: SortBy[sortBy],
      searchString,
      filter: Filters[filter],
      userId: request.user.userId,
    };

    return this.postService.getPostsPaginated(page, limit, where, response);
  }
}
