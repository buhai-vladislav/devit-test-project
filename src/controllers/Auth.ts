import { HttpStatus } from '@nestjs/common';
import { Body, Controller, Post, Res } from '@nestjs/common/decorators';
import { ApiBody, ApiOperation, ApiResponse, ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { CreateUserDto } from 'src/dtos/CreateUser';
import { LoginDto } from 'src/dtos/Login';
import { PublicRoute } from 'src/guards/PublicRoute';
import { AuthService } from 'src/services/Auth';

@ApiTags('Auth')
@Controller('/auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @ApiOperation({
    description: 'Endpoint for signup process of a new user.',
    operationId: 'authSignUp',
  })
  @ApiResponse({
    status: HttpStatus.CREATED,
    description: 'The user successfully created',
  })
  @ApiResponse({
    status: HttpStatus.BAD_REQUEST,
    description: "The user wasn't created, or something went wrong",
  })
  @ApiBody({ type: CreateUserDto })
  @PublicRoute()
  @Post('/signup')
  public async signup(
    @Body() createUserDto: CreateUserDto,
    @Res() response: Response,
  ) {
    return this.authService.signup(createUserDto, response);
  }

  @ApiOperation({
    description:
      'Endpoint for login login process by creadentials and getting token.',
    operationId: 'authSignin',
  })
  @ApiResponse({
    status: HttpStatus.OK,
    description: 'The user successfully login',
  })
  @ApiResponse({
    status: HttpStatus.UNAUTHORIZED,
    description: 'The user login or password is not correct',
  })
  @ApiBody({ type: LoginDto })
  @PublicRoute()
  @Post('/signin')
  public async signin(@Body() loginDto: LoginDto, @Res() response: Response) {
    return this.authService.signin(loginDto, response);
  }
}
