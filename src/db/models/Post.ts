import {
  PrimaryColumn,
  Entity,
  Column,
  ManyToOne,
  JoinColumn,
  Unique,
} from 'typeorm';
import { User } from './User';
import { VersionedEntity } from './VersionedEntity';

@Entity('posts')
@Unique('POST_ID', ['id'])
export class Post extends VersionedEntity {
  @PrimaryColumn({ type: 'varchar', length: 36, unique: true })
  id: string;

  @Column('varchar')
  title: string;

  @Column('varchar')
  link: string;

  @Column({
    type: 'text',
    array: true,
    default: {},
    nullable: true,
  })
  categories?: string[];

  @Column('varchar')
  description: string;

  @Column({
    type: 'varchar',
    length: 50,
    name: 'creator_name',
  })
  creatorName: string;

  @Column('varchar')
  thumbnail: string;

  @ManyToOne(() => User, (user) => user.posts, {
    nullable: true,
    cascade: true,
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'creator_id', referencedColumnName: 'id' })
  creator?: User;
}
