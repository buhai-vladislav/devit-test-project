import { UserRole } from 'src/types/UserRole';
import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { Post } from './Post';
import { VersionedEntity } from './VersionedEntity';

@Entity('users')
export class User extends VersionedEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    type: 'varchar',
    length: 50,
  })
  fullname: string;

  @Column({
    type: 'varchar',
    length: 255,
    unique: true,
  })
  email: string;

  @Column({
    type: 'varchar',
    length: 255,
    unique: true,
  })
  password: string;

  @Column('enum', {
    enum: UserRole,
    enumName: 'UserRole',
    default: UserRole.ADMIN,
  })
  role: UserRole;

  @OneToMany(() => Post, (posts) => posts.creator)
  posts?: Post[];
}
