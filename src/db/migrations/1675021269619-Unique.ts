import { MigrationInterface, QueryRunner } from 'typeorm';

export class Unique1675021269619 implements MigrationInterface {
  name = 'Unique1675021269619';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "posts" ADD CONSTRAINT "UQ_2829ac61eff60fcec60d7274b9e" UNIQUE ("id")`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE "posts" DROP CONSTRAINT "UQ_2829ac61eff60fcec60d7274b9e"`,
    );
  }
}
