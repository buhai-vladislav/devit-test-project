import { HttpStatus, Injectable } from '@nestjs/common';
import { Response } from 'express';
import { AffectedResponse } from 'src/types/AffectedResponse';
import ResponseUtils from 'src/utils/ResponseUtils';
import { Repository, DeepPartial, FindOptionsWhere } from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';

@Injectable()
export class BaseService<T> {
  constructor(protected readonly baseRepository: Repository<T>) {}

  public async create(createDto: DeepPartial<T>, response: Response) {
    let resultRes = null;
    try {
      resultRes = await this.baseRepository.save(createDto);
    } catch (error) {
      return ResponseUtils.sendError(response, HttpStatus.BAD_REQUEST, error);
    }

    return ResponseUtils.sendSuccess(
      response,
      HttpStatus.CREATED,
      `${this.baseRepository.metadata.name} record successfully created.`,
      resultRes,
    );
  }

  public async getOneByOptions(
    where: FindOptionsWhere<T>,
    response: Response,
    relations?: string[],
  ) {
    let resultRes = null;

    try {
      resultRes = await this.baseRepository.findOne({
        where,
        relations,
      });
    } catch (error) {
      return ResponseUtils.sendError(response, HttpStatus.BAD_REQUEST, error);
    }

    return ResponseUtils.sendSuccess(
      response,
      HttpStatus.OK,
      `${this.baseRepository.metadata.name} returned data.`,
      resultRes,
    );
  }

  public async update(
    recordId: string,
    updateDto: QueryDeepPartialEntity<T>,
    response: Response,
  ) {
    let resultRes = null;

    try {
      resultRes = await this.baseRepository.update(recordId, updateDto);
    } catch (error) {
      return ResponseUtils.sendError(response, HttpStatus.BAD_REQUEST, error);
    }

    if (!resultRes.affected) {
      return ResponseUtils.sendError(
        response,
        HttpStatus.BAD_REQUEST,
        `${this.baseRepository.metadata.name} record for update not found.`,
      );
    }

    const affectedResponseData: AffectedResponse = {
      isAffected: !!resultRes.affected,
    };

    return ResponseUtils.sendSuccess(
      response,
      HttpStatus.OK,
      `${this.baseRepository.metadata.name} record updated.`,
      affectedResponseData,
    );
  }

  public async delete(where: FindOptionsWhere<T>, response: Response) {
    let resultRes = null;

    try {
      resultRes = await this.baseRepository.delete(where);
    } catch (error) {
      return ResponseUtils.sendError(response, HttpStatus.BAD_REQUEST, error);
    }

    if (!resultRes.affected) {
      return ResponseUtils.sendError(
        response,
        HttpStatus.BAD_REQUEST,
        `${this.baseRepository.metadata.name} record for deleting not found.`,
      );
    }

    const affectedResponseData: AffectedResponse = {
      isAffected: !!resultRes.affected,
    };

    return ResponseUtils.sendSuccess(
      response,
      HttpStatus.OK,
      `${this.baseRepository.metadata.name} removed.`,
      affectedResponseData,
    );
  }
}
