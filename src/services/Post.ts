import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Response } from 'express';
import { Post } from 'src/db/models/Post';
import { User } from 'src/db/models/User';
import { CreatePostDto } from 'src/dtos/CreatePost';
import { IPagination } from 'src/types/Pagination';
import { Filters, IPostWhere } from 'src/types/PostsWhere';
import { getPaginationLimit } from 'src/utils/PaginationUtils';
import ResponseUtils from 'src/utils/ResponseUtils';
import { deletePropertyPath } from 'src/utils/Utils';
import { DeepPartial, Repository } from 'typeorm';
import { BaseService } from './BaseService';

const SAVED_POSTS_MESSAGE = 'Posts from rss parser successfully saved.';
const NO_POSTS_SAVED_MESSAGE = 'No post find to save.';

@Injectable()
export class PostService extends BaseService<Post> {
  constructor(
    @InjectRepository(Post) private readonly postRepository: Repository<Post>,
  ) {
    super(postRepository);
  }

  public async getPostsPaginated(
    page: number,
    limit: number,
    where: IPostWhere,
    response: Response,
  ) {
    const { filter, userId, searchString, sortBy } = where;
    const formatedLimit = getPaginationLimit(limit);
    const skip = page >= 1 ? (page - 1) * formatedLimit : formatedLimit;
    const removePropertyPaths: string[] = ['creator.password'];
    let responseData = null;

    const query = this.postRepository.createQueryBuilder('posts');

    if (filter === Filters.OWN_POSTS) {
      query
        .leftJoinAndSelect('posts.creator', 'creator')
        .where('creator.id = :id', { id: userId });
    }

    if (searchString) {
      query.andWhere(`SIMILARITY(posts.title,'${searchString}') > 0.3`);
    }

    try {
      responseData = await query
        .skip(skip)
        .take(formatedLimit)
        .orderBy('posts.createdAt', sortBy)
        .getManyAndCount();
    } catch (error) {
      return ResponseUtils.sendError(response, HttpStatus.BAD_REQUEST, error);
    }

    const [items, totalItems] = responseData;
    const totalPages = Math.ceil(totalItems / formatedLimit);

    // remove password property from returned items
    removePropertyPaths.forEach((path) =>
      items.forEach((elem: Post) => deletePropertyPath<Post>(elem, path)),
    );

    const resultData: IPagination<Post> = {
      items,
      meta: {
        currentPage: page,
        itemCount: items.length,
        itemsPerPage: limit,
        totalItems,
        totalPages,
      },
    };

    return ResponseUtils.sendSuccess(
      response,
      HttpStatus.OK,
      'Posts request successfully returned data.',
      resultData,
    );
  }

  public async createPost(
    userId: string,
    createPostDto: CreatePostDto,
    response: Response,
  ) {
    let responseData = null;
    const user = await User.findOne({ where: { id: userId } });

    if (!user) {
      return ResponseUtils.sendError(
        response,
        HttpStatus.NOT_FOUND,
        'User not found!',
      );
    }

    try {
      responseData = await this.postRepository.save({
        ...createPostDto,
        creatorName: user.fullname,
        creator: { id: user.id },
      });
    } catch (error) {
      return ResponseUtils.sendError(response, HttpStatus.BAD_REQUEST, error);
    }

    return ResponseUtils.sendSuccess(
      response,
      HttpStatus.CREATED,
      'Post successfully created.',
      responseData,
    );
  }

  public async createManyFromRss(posts: DeepPartial<Post>[], logger: Logger) {
    let isAnyPostSaved = false;

    try {
      for (let index = 0; index < posts.length; index++) {
        const post = await this.postRepository.findOne({
          where: { id: posts[index].id },
        });

        if (!post) {
          await this.postRepository.save(posts[index]);
          isAnyPostSaved = true;
        }
      }
    } catch (error) {
      logger.error(error);
    }

    const message = isAnyPostSaved
      ? SAVED_POSTS_MESSAGE
      : NO_POSTS_SAVED_MESSAGE;

    logger.log(message);
  }
}
