import { HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { CreateUserDto } from 'src/dtos/CreateUser';
import { UserService } from './User';
import { Response } from 'express';
import ResponseUtils from 'src/utils/ResponseUtils';
import { AuthUtils } from 'src/utils/AuthUtils';
import { User } from 'src/db/models/User';
import { LoginDto } from 'src/dtos/Login';
import { IJwtPayload } from 'src/types/JwtPayload';
import { HttpException } from '@nestjs/common/exceptions';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  public async signup(createUserDto: CreateUserDto, response: Response) {
    const { password, email } = createUserDto;
    let resultRes = null;

    try {
      const user = await User.findOne({ where: { email } });

      if (user) {
        throw new HttpException('User already exist.', HttpStatus.CONFLICT);
      }

      const passwordHash = await AuthUtils.generatePasswordHash(password);

      resultRes = await this.userService.create(
        { ...createUserDto, password: passwordHash },
        response,
      );
    } catch (error) {
      return this.errorHandler(error, response, HttpStatus.CONFLICT);
    }

    return resultRes;
  }

  private async validateUser(
    loginDto: LoginDto,
  ): Promise<Partial<User> | null> {
    const { email, password } = loginDto;

    const user = await User.findOne({ where: { email } });

    if (!user) {
      throw new HttpException(
        'Login or password is not correct.',
        HttpStatus.UNAUTHORIZED,
      );
    }

    const isPasswordValid = await AuthUtils.checkPassword(
      password,
      user.password,
    );

    if (isPasswordValid) {
      const { password, ...validatedUser } = user; // Remove password property from result

      return validatedUser;
    }

    return null;
  }

  private signToken(payload: IJwtPayload) {
    const token = this.jwtService.sign(payload);

    return token;
  }

  public async signin(loginDto: LoginDto, response: Response) {
    try {
      const validatedUser = await this.validateUser(loginDto);

      if (validatedUser !== null) {
        const { id: userId, email } = validatedUser;
        const payload: IJwtPayload = { userId, email };

        const token = this.signToken(payload);
        const user: Omit<Partial<User>, 'password'> = validatedUser;

        return ResponseUtils.sendSuccess(
          response,
          HttpStatus.OK,
          'Login is successfully.',
          { user, token },
        );
      }
    } catch (error) {
      return this.errorHandler(error, response, HttpStatus.NOT_FOUND);
    }

    return ResponseUtils.sendError(
      response,
      HttpStatus.UNAUTHORIZED,
      'Login or password is not correct.',
    );
  }

  private async errorHandler(
    error: object | string,
    response: Response,
    status: HttpStatus = HttpStatus.BAD_REQUEST,
  ) {
    let message: string | object = error;

    if (error instanceof HttpException) {
      message = error.message;
    }

    return ResponseUtils.sendError(response, status, message);
  }
}
