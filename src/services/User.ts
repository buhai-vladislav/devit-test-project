import { Injectable, HttpStatus } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Response } from 'express';
import { User } from 'src/db/models/User';
import { CreateUserDto } from 'src/dtos/CreateUser';
import ResponseUtils from 'src/utils/ResponseUtils';
import { Repository } from 'typeorm';
import { BaseService } from './BaseService';

@Injectable()
export class UserService extends BaseService<User> {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
  ) {
    super(userRepository);
  }

  public async create(createUserDto: CreateUserDto, response: Response) {
    let resultRes = null;

    try {
      const user: User = await this.userRepository.save(createUserDto);

      const { password, ...clearUser } = user; // remove password from returned data

      resultRes = clearUser;
    } catch (error) {
      if (error && error.parameters) {
        delete error.parameters; // remove sensitive data
      }

      return ResponseUtils.sendError(response, HttpStatus.BAD_REQUEST, error);
    }

    return ResponseUtils.sendSuccess(
      response,
      HttpStatus.CREATED,
      'User record successfylly created.',
      resultRes,
    );
  }
}
