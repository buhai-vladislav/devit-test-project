import { Injectable, Logger } from '@nestjs/common';
import { Cron, Timeout } from '@nestjs/schedule';
import Parser from 'rss-parser';
import { Post } from 'src/db/models/Post';
import { DeepPartial } from 'typeorm';
import { PostService } from './Post';

@Injectable()
export class TaskService {
  constructor(private readonly postService: PostService) {}
  private readonly logger = new Logger(TaskService.name);
  private readonly RSS_URL = 'https://lifehacker.com/rss';

  @Cron('*/30 * * * * *') // The task runs every 30 seconds. This period only for testing.
  async handlePostByCronInterval() {
    await this.parseRssAction();
  }

  @Timeout(0) // The task runs after the application starts.
  async handlePostsAfterAppStart() {
    await this.parseRssAction();
  }

  private async parseRssAction() {
    const posts = await this.parseRss();

    await this.postService.createManyFromRss(posts, this.logger);
  }

  private async parseRss(): Promise<DeepPartial<Post>[]> {
    const parser = new Parser({ customFields: { item: ['media:thumbnail'] } });

    const feed = await parser.parseURL(this.RSS_URL);
    const posts: DeepPartial<Post>[] = [];

    feed.items.forEach((item, index) => {
      posts.push({
        id: item?.guid,
        creatorName: item?.creator,
        description: item?.contentSnippet,
        categories: item?.categories,
        link: item?.link,
        title: item?.title,
        thumbnail: item['media:thumbnail']['$']?.url,
        createdAt: new Date(item.pubDate),
      });
    });

    return posts;
  }
}
