import { HttpStatus } from '@nestjs/common';
import { Response } from 'express';

export default class ResponseUtils {
  static sendError(
    response: Response,
    statusCode: HttpStatus,
    message: string | object,
  ) {
    return response.status(statusCode).json({
      statusCode,
      message,
    });
  }

  static sendSuccess<T>(
    response: Response,
    statusCode: HttpStatus,
    message: string | object,
    data?: Partial<T> | T,
  ) {
    return response.status(statusCode).json({
      statusCode,
      message,
      data,
    });
  }
}
