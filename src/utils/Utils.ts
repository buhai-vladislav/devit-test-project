export const deletePropertyPath = <T>(obj: T, path: string) => {
  let splitedPath: string[] = [];

  if (!obj || !path) {
    return;
  }

  if (typeof path === 'string') {
    splitedPath = path.split('.');
  }

  for (var i = 0; i < splitedPath.length - 1; i++) {
    obj = obj[splitedPath[i]];

    if (typeof obj === 'undefined' || obj === null) {
      return;
    }
  }

  delete obj[splitedPath.pop()];

  return obj;
};
