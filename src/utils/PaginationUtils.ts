import { DEFAULT_PAGINATION_LIMIT, PAGINATION_MAX_LIMIT } from './constants';

export const getPaginationLimit = (outerLimit: number): number => {
  const positiveLimit = outerLimit < 0 ? DEFAULT_PAGINATION_LIMIT : outerLimit;
  const limit =
    positiveLimit > PAGINATION_MAX_LIMIT ? PAGINATION_MAX_LIMIT : positiveLimit;

  return limit;
};
